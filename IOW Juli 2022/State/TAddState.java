package source.dataon.setting.system_setting.reference_data_setting.state;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddState extends BaseTest {
	
	private static String file_location = curr + "//test-data//Add State.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_AddState(
    		String TC_ID, String STATE_NAME, String COUNTRY_NAME, String TEST_CASE_TYPE)
    throws Exception
    {
        String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************DELETE STATE*************
            checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_11']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("System Setting"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Reference Data Setting"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            WebElement submenu3 = fluentWait(By.linkText("State"));
            Actions subactions3 = new Actions(driver);
            subactions3.moveToElement(submenu3).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | System Setting | Reference Data Setting | State", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu3);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);

            WebElement ADD = fluentWait(By.id("ADD"));
            ADD.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Add");
            driver.switchTo().parentFrame();
            Thread.sleep(5000);
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
            test.get().pass("Menampilkan Form Add Country", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());
            
            if (!STATE_NAME.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_state_name"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), STATE_NAME);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, STATE_NAME);
                test.get().pass("<small>[Textfield]</small> Country Code : " + val);
                Thread.sleep(500);
            }
            
            
            if(!COUNTRY_NAME.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_country_id"))).selectByVisibleText(COUNTRY_NAME);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Currency : " + COUNTRY_NAME);
            }
            
           
            WebElement submit = fluentWait(By.id("btn_a_1"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}
