package source.dataon.setting.system_setting.reference_data_setting.score_setting;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddScoreSettingListValue extends BaseTest {
	 private static String file_location = curr + "//test-data//Add Score Setting List.xls";
	    private static String sheet_name = "Test Data";
	    private static ReadExcel re = new ReadExcel();

	    @Test(dataProvider = "ReadData")
	    public void TC_AddScoreSettingListValue(
	    		String TC_ID, String SCORE_CODE, String SCORE_TYPE, String SCORE_VALUE, 
	    		String SCORE_LIST_VALUE, String SCORE_MASK, String DESC, 
	    		String SCORE_LIST_VALUE1, String SCORE_MASK1, String DESC1, String TEST_CASE_TYPE)
	    throws Exception
	    {
	        String SC_TYPE = "vertical"; /*nonreport*/
	        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
	        try
	        {
	        	//*************Add SCORE SETTING LIST VALUE*************
	            checkPageIsReady();
	            driver.switchTo().defaultContent();
	            Thread.sleep(3000);
	            WebDriverWait wait = new WebDriverWait(driver, 120);
	            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_8']//a[@class='menulink']")));
	            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
	            Actions actions = new Actions(driver);
	            actions.moveToElement(menu).perform();
	            Thread.sleep(500);
	            WebElement submenu1 = fluentWait(By.linkText("System Setting"));
	            Actions subactions1 = new Actions(driver);
	            subactions1.moveToElement(submenu1).perform();
	            Thread.sleep(500);
	            WebElement submenu2 = fluentWait(By.linkText("Reference Data Setting"));
	            Actions subactions2 = new Actions(driver);
	            subactions2.moveToElement(submenu2).perform();
	            Thread.sleep(500);
	            WebElement submenu3 = fluentWait(By.linkText("Score Setting"));
	            Actions subactions3 = new Actions(driver);
	            subactions3.moveToElement(submenu3).perform();
	            Thread.sleep(500);
	            JavascriptExecutor executor = (JavascriptExecutor) driver;
	            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
	            test.get().pass("Menuju ke menu Setting | System Setting | Reference Data Setting | SCORE SETTING", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
	            executor.executeScript("arguments[0].click();", submenu3);
	            checkPageIsReady();
	            Thread.sleep(5000);
	            driver.switchTo().parentFrame();
	            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
	            //fluentWait(By.id("RELOAD")).click();
	            Thread.sleep(8000);

	            WebElement ADD = fluentWait(By.id("ADD"));
	            ADD.click();
	            Thread.sleep(5000);
	            test.get().pass("<small>[button]</small> Klik tombol Add");
	            driver.switchTo().parentFrame();
	            Thread.sleep(8000);
	            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
	            test.get().pass("Menampilkan Form Add Score Setting", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());

	            if(!SCORE_CODE.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_score_code")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SCORE_CODE);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, SCORE_CODE);
		         	    test.get().pass("<small>[Textfield]</small> Score Code : " + val);
		         	    Thread.sleep(500);
		         	}
	            

	            if(!SCORE_TYPE.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_score_desc")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SCORE_TYPE);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, SCORE_TYPE);
		         	    test.get().pass("<small>[Textfield]</small> Score Type : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            if(!SCORE_VALUE.equalsIgnoreCase(""))
	            {
	                WebElement x = fluentWait(By.xpath("//input[@id='inp_score_type' and @title='" + SCORE_VALUE + "']"));
	                x.click();
	                String value = x.getAttribute("title");
	                Assert.assertEquals(value, SCORE_VALUE);
	                test.get().pass("<small>[Radiobutton]</small> Score Value : " + value);
	                Thread.sleep(500);
	            }
	            
	            if(!SCORE_LIST_VALUE.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("txtHour_1")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SCORE_LIST_VALUE);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, SCORE_LIST_VALUE);
		         	    test.get().pass("<small>[Textfield]</small> (List Value) Score Value : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            if(!SCORE_MASK.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("txtVal_1")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SCORE_MASK);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, SCORE_MASK);
		         	    test.get().pass("<small>[Textfield]</small> (List Value) Score Mask : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            if(!DESC.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("txtVal1_1")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), DESC);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, DESC);
		         	    test.get().pass("<small>[Textfield]</small> (List Value) Description : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            WebElement addRow = fluentWait(By.xpath("//img[@src='/sf6lib/images/icons/acssadd.png']"));
				addRow.click();
				Thread.sleep(1000);

	            
	            if(!SCORE_LIST_VALUE1.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("txtHour_2")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SCORE_LIST_VALUE1);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, SCORE_LIST_VALUE1);
		         	    test.get().pass("<small>[Textfield]</small> (List Value) Score Value : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            if(!SCORE_MASK1.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("txtVal_2")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SCORE_MASK1);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, SCORE_MASK1);
		         	    test.get().pass("<small>[Textfield]</small> (List Value) Score Mask : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            if(!DESC1.equalsIgnoreCase("")) {
		         	   WebElement element = (new WebDriverWait(driver, 10))
		         	    .until(ExpectedConditions.elementToBeClickable(By.id("txtVal1_2")));
		         	    element.clear();
		         	    Thread.sleep(500);
		         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), DESC1);
		         	    String val = element.getAttribute("value");
		         	    Assert.assertEquals(val, DESC1);
		         	    test.get().pass("<small>[Textfield]</small> (List Value) Description : " + val);
		         	    Thread.sleep(500);
		         	}
	            
	            
	            
	            WebElement submit = fluentWait(By.id("btn_a_1"));
	            submit.click();
	            Thread.sleep(4000);
	            test.get().pass("<small>[Button]</small> Mengklik Submit");
	            //handle alert
	            resolveAllAlerts(driver, 16, true);
	            Thread.sleep(4000);

	        }
	        catch (ElementNotVisibleException e) {
	            Assert.fail("Not Found Element/Page");
	        }
	    }

	    @DataProvider(name = "ReadData")
	    private static Object[][] readData() {
	        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
	        return arrObject;
	    }
}