package source.dataon.setting.system_setting.reference_data_setting.bank;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TEditBank extends BaseTest {
    private static String file_location = curr + "//test-data//Edit Bank.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_EditBank
    (String TC_ID, String BANK_CODE, String BANK_NAME, 
			String BANK_GROUP, String BANK_BRANCH_CODE, String BANK_BRANCH, 
			String BANK_ADDRESS, String BANK_PHONE, String BI_CODE, String CLEARING_CODE,
			String BB_SUB_CODE, String AB_CODE, String RTGS_CODE, String TEST_CASE_TYPE)
    throws Exception
    {
        String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
            //*************EDIT BANK*************
        	checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_8']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("System Setting"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Reference Data Setting"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            WebElement submenu3 = fluentWait(By.linkText("Bank"));
            Actions subactions3 = new Actions(driver);
            subactions3.moveToElement(submenu3).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | System Setting | Reference Data Setting | Bank", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu3);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);

//			//			 Search Target Name
			fluentWait(By.id("header_ardata_1_1")).click(); //Klik Subject
			Thread.sleep(1500);

			WebElement searchname = fluentWait(By.id("txtSearch_1")); //Search Subject
			searchname.sendKeys("");
			searchname.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BANK_CODE); //Mencari Subject sesuai xls
			Thread.sleep(500);

			searchname.sendKeys(Keys.ENTER); // Klik Enter
			Thread.sleep(5000);

			test.get().pass("Mengisi data pencarian"); // Report
			String searchnameval = searchname.getAttribute("value");
			Assert.assertEquals(searchnameval, BANK_CODE);
			test.get().pass("<small>[Input Textfield]</small> Data Pencarian : " + searchnameval); // Report
			fluentWait(By.id("header_ardata_1_1")).click();
			Thread.sleep(5000);
			
			WebElement linkno = fluentWait(By.linkText(BANK_CODE)); //Klik Currency Code
			linkno.click();
			Thread.sleep(5000);

			test.get().pass("Klik Bank Code :" + linkno.getText());// Report
			Thread.sleep(5000);

			driver.switchTo().parentFrame();
			Thread.sleep(5000);

			takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2"); // Report
			test.get().pass("Menampilkan Edit Bank", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());
            

            if(!BANK_NAME.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_bank_name")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BANK_NAME);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BANK_NAME);
                        test.get().pass("<small>[Textfield]</small> Bank Name : " + val);
                        Thread.sleep(500);
            }
            
            if(!BANK_GROUP.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_bankgroup_code"))).selectByVisibleText(BANK_GROUP);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Bank Group : " + BANK_GROUP);
            }
            
            if(!BANK_BRANCH_CODE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_branch_code")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BANK_BRANCH_CODE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BANK_BRANCH_CODE);
                        test.get().pass("<small>[Textfield]</small> Bank Branch Code : " + val);
                        Thread.sleep(500);
            }
            
            if(!BANK_BRANCH.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_bank_branch")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BANK_BRANCH);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BANK_BRANCH);
                        test.get().pass("<small>[Textfield]</small> Bank Branch : " + val);
                        Thread.sleep(500);
            }
            
            if(!BANK_ADDRESS.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_bank_address")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BANK_ADDRESS);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BANK_ADDRESS);
                        test.get().pass("<small>[Textfield]</small> Bank Address : " + val);
                        Thread.sleep(500);
            }
            
            if(!BANK_PHONE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_bank_phone")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BANK_PHONE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BANK_PHONE);
                        test.get().pass("<small>[Textfield]</small> Bank Phone : " + val);
                        Thread.sleep(500);
            }
            
            if(!BI_CODE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_bi_code")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BI_CODE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BI_CODE);
                        test.get().pass("<small>[Textfield]</small> BI Code : " + val);
                        Thread.sleep(500);
            }
            
            
            if(!CLEARING_CODE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_clr_code")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), CLEARING_CODE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, CLEARING_CODE);
                        test.get().pass("<small>[Textfield]</small> Clearing Code : " + val);
                        Thread.sleep(500);
            }
            
            if(!BB_SUB_CODE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_branch_scode")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), BB_SUB_CODE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, BB_SUB_CODE);
                        test.get().pass("<small>[Textfield]</small> Bank Branch Sub-Code : " + val);
                        Thread.sleep(500);
            }
            
            if(!AB_CODE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_atm_bersama_code")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), AB_CODE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, AB_CODE);
                        test.get().pass("<small>[Textfield]</small> ATM Bersama Code : " + val);
                        Thread.sleep(500);
            }
            
            if(!RTGS_CODE.equalsIgnoreCase(""))
            {
            	WebElement element = (new WebDriverWait(driver, 10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("inp_rtgs_code")));
                        element.clear();
                        Thread.sleep(500);
                        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), RTGS_CODE);
                        String val = element.getAttribute("value");
                        Assert.assertEquals(val, RTGS_CODE);
                        test.get().pass("<small>[Textfield]</small> RTGS Code : " + val);
                        Thread.sleep(500);
            }
            
            WebElement submit = fluentWait(By.id("btn_a_2"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}
