package source.dataon.setting.system_setting.reference_data_setting.insurance;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddInsurance extends BaseTest {
	 private static String file_location = curr + "//test-data//Add Insurance.xls";
	    private static String sheet_name = "Test Data";
	    private static ReadExcel re = new ReadExcel();

	    @Test(dataProvider = "ReadData")
	    public void TC_AddInsurance(
	    		String TC_ID, String INS_CODE, String INS_NAME, String INS_ADDRESS,
	    		String CITY, String TEST_CASE_TYPE)
	    throws Exception
	    {
	        String SC_TYPE = "vertical"; /*nonreport*/
	        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
	        try
	        {
	        	//*************ADD INSURANCE*************
	            checkPageIsReady();
	            driver.switchTo().defaultContent();
	            Thread.sleep(3000);
	            WebDriverWait wait = new WebDriverWait(driver, 120);
	            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_8']//a[@class='menulink']")));
	            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
	            Actions actions = new Actions(driver);
	            actions.moveToElement(menu).perform();
	            Thread.sleep(500);
	            WebElement submenu1 = fluentWait(By.linkText("System Setting"));
	            Actions subactions1 = new Actions(driver);
	            subactions1.moveToElement(submenu1).perform();
	            Thread.sleep(500);
	            WebElement submenu2 = fluentWait(By.linkText("Reference Data Setting"));
	            Actions subactions2 = new Actions(driver);
	            subactions2.moveToElement(submenu2).perform();
	            Thread.sleep(500);
	            WebElement submenu3 = fluentWait(By.linkText("Insurance"));
	            Actions subactions3 = new Actions(driver);
	            subactions3.moveToElement(submenu3).perform();
	            Thread.sleep(500);
	            JavascriptExecutor executor = (JavascriptExecutor) driver;
	            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
	            test.get().pass("Menuju ke menu Setting | System Setting | Reference Data Setting | Insurance", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
	            executor.executeScript("arguments[0].click();", submenu3);
	            checkPageIsReady();
	            Thread.sleep(5000);
	            driver.switchTo().parentFrame();
	            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
	            //fluentWait(By.id("RELOAD")).click();
	            Thread.sleep(8000);

	            WebElement ADD = fluentWait(By.id("ADD"));
	            ADD.click();
	            Thread.sleep(5000);
	            test.get().pass("<small>[button]</small> Klik tombol Add");
	            driver.switchTo().parentFrame();
	            Thread.sleep(8000);
	            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
	            test.get().pass("Menampilkan Form Add Insurance", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());

	            if(!INS_CODE.equalsIgnoreCase("")) {
	         	   WebElement element = (new WebDriverWait(driver, 10))
	         	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_institution_code")));
	         	    element.clear();
	         	    Thread.sleep(500);
	         	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), INS_CODE);
	         	    String val = element.getAttribute("value");
	         	    Assert.assertEquals(val, INS_CODE);
	         	    test.get().pass("<small>[Textfield]</small> Insurance Code : " + val);
	         	    Thread.sleep(500);
	         	}
	            
	            if(!INS_NAME.equalsIgnoreCase("")) {
	        	   WebElement element = (new WebDriverWait(driver, 10))
	        	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_institution_name")));
	        	    element.clear();
	        	    Thread.sleep(500);
	        	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), INS_NAME);
	        	    String val = element.getAttribute("value");
	        	    Assert.assertEquals(val, INS_NAME);
	        	    test.get().pass("<small>[Textfield]</small> Insurance Name : " + val);
	        	    Thread.sleep(500);
	        	}
	            
	            if(!INS_ADDRESS.equalsIgnoreCase("")) {
		        	   WebElement element = (new WebDriverWait(driver, 10))
		        	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_institution_address")));
		        	    element.clear();
		        	    Thread.sleep(500);
		        	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), INS_ADDRESS);
		        	    String val = element.getAttribute("value");
		        	    Assert.assertEquals(val, INS_ADDRESS);
		        	    test.get().pass("<small>[Textfield]</small> Insurance Address : " + val);
		        	    Thread.sleep(500);
		        	}
	            
	            if(!CITY.equalsIgnoreCase("")) {
	            	 WebElement inp_city_id = fluentWait(By.id("inp_city_id"));
	 			    inp_city_id.clear();
	 			    inp_city_id.sendKeys(CITY);
	 			    String inp_city_idval = inp_city_id.getAttribute("value");
	 			    try
	 			    {
	 			        Assert.assertEquals(inp_city_idval, CITY);
	 			        test.get().pass("<small>[Input Textfield or Suggestion Tip]</small> City : " + inp_city_idval);
	 			    }
	 			    catch (AssertionError e)
	 			    {
	 			        test.get().fail("<small>[Input Textfield or Suggestion Tip]</small> City : " + inp_city_idval);
	 			        throw e;
	 			    }
	 			    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(., '" + CITY + "')]")));
	 			    WebElement city = fluentWait(By.xpath("//td[contains(., '" + CITY + "')]"));
	 			    city.click();
	 			    Thread.sleep(500);
		        	}
	            
	            
	            
	            WebElement submit = fluentWait(By.id("btn_a_1"));
	            submit.click();
	            Thread.sleep(4000);
	            test.get().pass("<small>[Button]</small> Mengklik Submit");
	            //handle alert
	            resolveAllAlerts(driver, 16, true);
	            Thread.sleep(4000);

	        }
	        catch (ElementNotVisibleException e) {
	            Assert.fail("Not Found Element/Page");
	        }
	    }

	    @DataProvider(name = "ReadData")
	    private static Object[][] readData() {
	        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
	        return arrObject;
	    }
	        

}
