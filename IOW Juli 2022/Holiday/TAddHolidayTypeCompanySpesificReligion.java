package source.dataon.setting.system_setting.reference_data_setting.holiday;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddHolidayTypeCompanySpesificReligion extends BaseTest {
	 private static String file_location = curr + "//test-data//Add Holiday Type Company Spesific Religion.xls";
	    private static String sheet_name = "Test Data";
	    private static ReadExcel re = new ReadExcel();

	    @Test(dataProvider = "ReadData")
	    public void TC_AddHolidayTypeCompanySpesificReligion(
	    		String TC_ID, String LANG, String NAME, String SDATE, 
	    		String EDATE, String HOLIDAY_TYPE, String COMPANY, String SR, String RELIGION, String TEST_CASE_TYPE)
	    throws Exception
	    {
	        String SC_TYPE = "vertical"; /*nonreport*/
	        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
	        try
	        {
	        	//*************Add Holiday Type Company Spesific Religion*************
	            checkPageIsReady();
	            driver.switchTo().defaultContent();
	            Thread.sleep(3000);
	            WebDriverWait wait = new WebDriverWait(driver, 120);
	            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_8']//a[@class='menulink']")));
	            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
	            Actions actions = new Actions(driver);
	            actions.moveToElement(menu).perform();
	            Thread.sleep(500);
	            WebElement submenu1 = fluentWait(By.linkText("System Setting"));
	            Actions subactions1 = new Actions(driver);
	            subactions1.moveToElement(submenu1).perform();
	            Thread.sleep(500);
	            WebElement submenu2 = fluentWait(By.linkText("Reference Data Setting"));
	            Actions subactions2 = new Actions(driver);
	            subactions2.moveToElement(submenu2).perform();
	            Thread.sleep(500);
	            WebElement submenu3 = fluentWait(By.linkText("Holiday"));
	            Actions subactions3 = new Actions(driver);
	            subactions3.moveToElement(submenu3).perform();
	            Thread.sleep(500);
	            JavascriptExecutor executor = (JavascriptExecutor) driver;
	            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
	            test.get().pass("Menuju ke menu Setting | System Setting | Reference Data Setting | Holiday", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
	            executor.executeScript("arguments[0].click();", submenu3);
	            checkPageIsReady();
	            Thread.sleep(5000);
	            driver.switchTo().parentFrame();
	            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
	            //fluentWait(By.id("RELOAD")).click();
	            Thread.sleep(8000);

	            WebElement ADD = fluentWait(By.id("ADD"));
	            ADD.click();
	            Thread.sleep(5000);
	            test.get().pass("<small>[button]</small> Klik tombol Add");
	            driver.switchTo().parentFrame();
	            Thread.sleep(8000);
	            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
	            test.get().pass("Menampilkan Form Add Holiday", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());

	            
	            if (!NAME.equalsIgnoreCase("")) {
	                String[] st_desc = NAME.split(", ");
	                String[] lang = LANG.split(", ");
	                for(int i = 0;i < st_desc.length;i++)
	                {
	                    WebElement element = (new WebDriverWait(driver, 10))
	                    .until(ExpectedConditions.elementToBeClickable(By.id("inp_holiday_name_"+ lang[i])));
	                    element.clear();
	                    Thread.sleep(500);
	                    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), st_desc[i]);
	                    Thread.sleep(500);
	                    String val = element.getAttribute("value");
	                    Assert.assertEquals(val, st_desc[i]);
	                    test.get().pass("<small>[Input Textfield]</small> Holiday ["+lang[i]+"]: " + st_desc[i]);
	                }
	                Thread.sleep(1000);
	            }
				
				if(!SDATE.equalsIgnoreCase("")) {
	        	    WebElement element = (new WebDriverWait(driver, 10))
	        	    .until(ExpectedConditions.elementToBeClickable(By.id("cal_start_date")));
	        	    element.clear();
	        	    Thread.sleep(500);
	        	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), SDATE);
	        	    String val = element.getAttribute("value");
	        	    Assert.assertEquals(val, SDATE);
	        	    test.get().pass("<small>[Textfield]</small> Holiday Start Date : " + val);
	        	    Thread.sleep(500);
	        	}
	            
	            if(!EDATE.equalsIgnoreCase("")) {
	        	    WebElement element = (new WebDriverWait(driver, 10))
	        	    .until(ExpectedConditions.elementToBeClickable(By.id("cal_end_date")));
	        	    element.clear();
	        	    Thread.sleep(500);
	        	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), EDATE);
	        	    String val = element.getAttribute("value");
	        	    Assert.assertEquals(val, EDATE);
	        	    test.get().pass("<small>[Textfield]</small>Holiday End Date : " + val);
	        	    Thread.sleep(500);
	        	}
	            
	            
	            if(!HOLIDAY_TYPE.equalsIgnoreCase(""))
	            {
	                WebElement x = fluentWait(By.xpath("//input[@id='inp_holiday_type' and @title='" + HOLIDAY_TYPE + "']"));
	                x.click();
	                String value = x.getAttribute("title");
	                Assert.assertEquals(value, HOLIDAY_TYPE);
	                test.get().pass("<small>[Radiobutton]</small> Holiday Type : " + value);
	                Thread.sleep(500);
	            }
	            
	            if(!COMPANY.equalsIgnoreCase(""))
	            {
	                new Select(fluentWait(By.name("inp_company_id"))).selectByVisibleText(COMPANY);
	                Thread.sleep(500);
	                test.get().pass("<small>[Selectbox]</small> Company : " + COMPANY);
	            }
		            
	            
	            if(SR.equalsIgnoreCase("Y"))
	            {
	                WebElement x = fluentWait(By.id("inp_is_religion"));
	                if(!x.isSelected())
	                {
	                    x.click();
	                }
	                test.get().pass("<small>[Checkbox]</small> Apply to Spesific Religion: " + SR);
	                Thread.sleep(500);
	            }	        
		        
	            if (!RELIGION.equalsIgnoreCase("")) {
		        
		        String[] typeofL = RELIGION.split(", ");
	            int num = 1;
	            for (String eltypeofL : typeofL) {
	                if (eltypeofL != null && eltypeofL.length() > 0)
	                {
	                    WebElement tol = fluentWait(By.id("inp_lstreligion"));
	                    tol.sendKeys("");
	                    tol.sendKeys(eltypeofL);
	                    Thread.sleep(5000);
	                    WebElement found = fluentWait(By.cssSelector("#unselinp_lstreligion > option:nth-child(1)"));
	                    actions.doubleClick(found).perform();
	                    Thread.sleep(3000);
	                    tol.clear();
	                    Thread.sleep(500);
	                    test.get().pass("<small>[Multiple Selectbox]</small> Spesific Religion " + num + " : " + eltypeofL);
	                }
	                num++;
	            	}
	            }
	            Thread.sleep(1000);
	           
	            
	            WebElement submit = fluentWait(By.id("btn_a_1"));
	            submit.click();
	            Thread.sleep(4000);
	            test.get().pass("<small>[Button]</small> Mengklik Submit");
	            //handle alert
	            resolveAllAlerts(driver, 16, true);
	            Thread.sleep(4000);

	        }
	        catch (ElementNotVisibleException e) {
	            Assert.fail("Not Found Element/Page");
	        }
	    }

	    @DataProvider(name = "ReadData")
	    private static Object[][] readData() {
	        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
	        return arrObject;
	    }

}
