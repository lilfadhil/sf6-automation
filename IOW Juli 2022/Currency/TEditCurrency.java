package source.dataon.setting.system_setting.reference_data_setting.currency;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TEditCurrency extends BaseTest {
    private static String file_location = curr + "//test-data//Edit Currency.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_EditCurrency
    	(String TC_ID, String CURRENCY_CODE, String CURRENCY_SYMBOL, 
    			String DESCRIPTION, String ACTIVE_STATUS, String DECIMAL_DIGIT,
    			String TEST_CASE_TYPE)
    throws Exception
    {
        String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
            //*************EDIT CURRENCY*************
        	checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_8']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("System Setting"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Reference Data Setting"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            WebElement submenu3 = fluentWait(By.linkText("Currency"));
            Actions subactions3 = new Actions(driver);
            subactions3.moveToElement(submenu3).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | System Setting | Reference Data Setting | Currency", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu3);
            checkPageIsReady();
            Thread.sleep(5000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(8000);

//			 Search Target Name
			fluentWait(By.id("header_ardata_1_1")).click(); //Klik Subject
			Thread.sleep(1500);

			WebElement searchname = fluentWait(By.id("txtSearch_1")); //Search Subject
			searchname.sendKeys("");
			searchname.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), CURRENCY_CODE); //Mencari Subject sesuai xls
			Thread.sleep(500);

			searchname.sendKeys(Keys.ENTER); // Klik Enter
			Thread.sleep(5000);

			test.get().pass("Mengisi data pencarian"); // Report
			String searchnameval = searchname.getAttribute("value");
			Assert.assertEquals(searchnameval, CURRENCY_CODE);
			test.get().pass("<small>[Input Textfield]</small> Data Pencarian : " + searchnameval); // Report
			fluentWait(By.id("header_ardata_1_1")).click();
			Thread.sleep(5000);
			
			WebElement linkno = fluentWait(By.linkText(CURRENCY_CODE)); //Klik Currency Code
			linkno.click();
			Thread.sleep(5000);

			test.get().pass("Klik Currency Code :" + linkno.getText());// Report
			Thread.sleep(5000);

			driver.switchTo().parentFrame();
			Thread.sleep(5000);

			takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2"); // Report
			test.get().pass("Menampilkan Edit Currency", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());
    
            if (!CURRENCY_SYMBOL.equalsIgnoreCase("")){
            	WebElement element = (new WebDriverWait(driver, 10))
                	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_currency_symbol")));
                	    element.clear();
                	    Thread.sleep(500);
                	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), CURRENCY_SYMBOL);
                	    String val = element.getAttribute("value");
                	    Assert.assertEquals(val, CURRENCY_SYMBOL);
                	    test.get().pass("<small>[Textfield]</small> Currency Symbol : " + val);
                	    Thread.sleep(500);
            }
            
            if (!DESCRIPTION.equalsIgnoreCase("")){
            	WebElement element = (new WebDriverWait(driver, 10))
                	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_description")));
                	    element.clear();
                	    Thread.sleep(500);
                	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), DESCRIPTION);
                	    String val = element.getAttribute("value");
                	    Assert.assertEquals(val, DESCRIPTION);
                	    test.get().pass("<small>[Textfield]</small> Description : " + val);
                	    Thread.sleep(500);
            }
            
            
            if(ACTIVE_STATUS.equalsIgnoreCase("N")) {
            	WebElement check = driver.findElement(By.id("inp_status"));
            	check.click();
            	test.get().pass("<small>[Checkbox]</small> Active: N");
            } else {
            	test.get().pass("<small>[Checkbox]</small> Active: Y");
            }
            
            if (!DECIMAL_DIGIT.equalsIgnoreCase("")){
            	WebElement element = (new WebDriverWait(driver, 10))
                	    .until(ExpectedConditions.elementToBeClickable(By.id("inp_decimal_rounding")));
                	    element.clear();
                	    Thread.sleep(500);
                	    element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), DECIMAL_DIGIT);
                	    String val = element.getAttribute("value");
                	    Assert.assertEquals(val, DECIMAL_DIGIT);
                	    test.get().pass("<small>[Textfield]</small> Decimal Digit : " + val);
                	    Thread.sleep(500);
            }
            

            WebElement submit = fluentWait(By.id("btn_a_2"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }


}
