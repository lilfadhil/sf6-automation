package source.dataon.timeatt.overtime_request;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddMultipleOvertimeStatic extends BaseTest {
    private static String file_location = curr + "//test-data//Add Overtime Multiple Overtime Static.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_AddMultipleOvertimeStatic
    	(String TC_ID, String REQUEST_TYPE, String REMARK, String OVERTIME_TYPE, String OPTION, String START, String TO,
    			String TEST_CASE_TYPE)
    throws Exception
    {
    	String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************ADD MASTER TASK*************
        	checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_3']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_3']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("Time"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Overtime"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            WebElement submenu3 = fluentWait(By.linkText("Overtime Request"));
            Actions subactions3 = new Actions(driver);
            subactions3.moveToElement(submenu3).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | Time & Attendance Setting | Leave Setting", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu3);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);

            WebElement ADD = fluentWait(By.id("ADD"));
            ADD.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Add");
            Thread.sleep(5000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);
            
                        
            if(!REQUEST_TYPE.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_reqtype' and @title='" + REQUEST_TYPE + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, REQUEST_TYPE);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }
            
            if(!REMARK.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_remark"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), REMARK);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, REMARK);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }
          
            if(!OVERTIME_TYPE.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_ovttype' and @title='" + OVERTIME_TYPE + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, OVERTIME_TYPE);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }
            
            if(!OPTION.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_dt' and @title='" + OPTION + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, OPTION);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }
            
            WebElement DISPLAYING = fluentWait(By.cssSelector("#tdb_1 > input[type=Button]:nth-child(11)"));
            DISPLAYING.click();
            Thread.sleep(2000);
                                  
            if (!START.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_ovttimefrom1"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), START);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, START);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }      
            
            if (!TO.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_ovttimeto1"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), TO);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, TO);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }

			Thread.sleep(1000);
            
            WebElement submit = fluentWait(By.id("btn_a_3"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}

