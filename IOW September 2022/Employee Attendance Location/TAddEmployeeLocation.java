package source.dataon.timeatt.employee_attendance_location;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddEmployeeLocation extends BaseTest {
    private static String file_location = curr + "//test-data//Add Employee Attendance.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_AddEmployeeLocation
    	(String TC_ID, String LOCATION_NAME, String START_DATE, String END_DATE, String EMPLOYEE,
    			String TEST_CASE_TYPE)
    throws Exception
    {
    	String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************ADD MASTER TASK*************
        	checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_2']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_2']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("Time"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Employee Attendance Location"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
            test.get().pass("Menuju ke menu Time | Employee Attendance Location", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu2);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);
            
            WebElement ADD = fluentWait(By.id("ADD"));
            ADD.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Add");
            Thread.sleep(5000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);
            
            if(!LOCATION_NAME.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_location_code"))).selectByVisibleText(LOCATION_NAME);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Location Name : " + LOCATION_NAME);
            }                                 
                                  
            if (!START_DATE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("cal_start_date"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), START_DATE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, START_DATE);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }      
            
            if (!END_DATE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("cal_end_date"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), END_DATE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, END_DATE);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }
            
            if (!EMPLOYEE.equalsIgnoreCase("")) {

				String[] typeofL = EMPLOYEE.split(", ");
				int num = 1;
				for (String eltypeofL : typeofL) {
					if (eltypeofL != null && eltypeofL.length() > 0) {
						WebElement tol = fluentWait(By.id("inp_empattendancelocation"));
						tol.sendKeys("");
						tol.sendKeys(eltypeofL);
						Thread.sleep(5000);
						WebElement found = fluentWait(By.cssSelector("#unselinp_empattendancelocation > option:nth-child(1)"));
						actions.doubleClick(found).perform();
						Thread.sleep(3000);
						tol.clear();
						Thread.sleep(500);
						test.get().pass("<small>[Multiple Selectbox]</small> Employee " + num + " : " + eltypeofL);
					}
					num++;
				}
			}

			Thread.sleep(1000);
            
            WebElement submit = fluentWait(By.id("btn_a_1"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}

