package source.dataon.employee.employee_attendance_location_upload;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class OnboardingLoadSearch extends BaseTest {
    private static String file_location = curr + "//test-data//Onboarding Load Search.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_OnboardingLoadSearch
    	(String TC_ID, String START_DATE, String END_DATE,
    			String TEST_CASE_TYPE)
    throws Exception
    {
    	String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************ADD MASTER TASK*************
            checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_11']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("Employee"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Onboarding"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Employee | Onboarding ", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu2);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);
            
            WebElement FILTER = fluentWait(By.cssSelector("#toolbarlist > div.toolbar.sprite-toolbar-filter"));
            FILTER.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Filter");
            Thread.sleep(5000);
                       
            if (!START_DATE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("cal_start_date"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), START_DATE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, START_DATE);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }      
            
            if (!END_DATE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("cal_end_date"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), END_DATE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, END_DATE);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            } 
            
            
            WebElement submit = fluentWait(By.id("btn_a_0"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Upload");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}

