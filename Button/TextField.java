
if(!REMIND_LATTER.equalsIgnoreCase("")) {
				    WebElement x = fluentWait(By.id("cal_remind_date"));
				    x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), REMIND_LATTER);
				    String val = x.getAttribute("value");
				    Assert.assertEquals(val, REMIND_LATTER);
				    test.get().pass("<small>[Textfield]</small> Date : " + val);
				    Thread.sleep(500);
				}
