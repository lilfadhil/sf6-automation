if (!END_DATE.equalsIgnoreCase("")) {
				WebElement x = fluentWait(By.id("cal_enddate"));
				x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), END_DATE);
				String val = x.getAttribute("value");
				Assert.assertEquals(val, END_DATE);
				test.get().pass("<small>[Date Field]</small> End Date : " + val);
				Thread.sleep(1000);
			}