if(!REPORT_TYPE.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_report_type' and @title='" + REPORT_TYPE + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, REPORT_TYPE);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }