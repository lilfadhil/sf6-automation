package source.dataon.setting.timeatt_setting.attendance_location;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddAttendanceLocation extends BaseTest {
    private static String file_location = curr + "//test-data//Add Attendance Location.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_AddAttendanceLocation
    	(String TC_ID, String LOCATION_CODE, String LOCATION_NAME, String LOCATION_ADDRESS, String CITY, String MAXIMUM_RADIUS, String TIME_SETTING, String DESTINATION,
    			String TEST_CASE_TYPE)
    throws Exception
    {
    	String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************ADD MASTER TASK*************
            checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_11']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("Time & Attendance Setting"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Attendance Location"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | Time & Attendance Setting | Attendance Location", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu2);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);

            WebElement ADD = fluentWait(By.id("ADD"));
            ADD.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Add");
            driver.switchTo().parentFrame();
            Thread.sleep(5000);
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
            test.get().pass("Menampilkan Form Attendance Location", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());
            
            if (!LOCATION_CODE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_location_code"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), LOCATION_CODE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, LOCATION_CODE);
                test.get().pass("<small>[Textfield]</small>Location Code : " + val);
                Thread.sleep(500);
            }
            
            if (!LOCATION_NAME.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_location_name"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), LOCATION_NAME);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, LOCATION_NAME);
                test.get().pass("<small>[Textfield]</small>Location Name : " + val);
                Thread.sleep(500);
            }
            
            if (!LOCATION_ADDRESS.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_location_address"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), LOCATION_ADDRESS);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, LOCATION_ADDRESS);
                test.get().pass("<small>[Textfield]</small>Leave Grade Code : " + val);
                Thread.sleep(500);
            }
            
            if (!CITY.equalsIgnoreCase("")) {
				WebElement x = fluentWait(By.id("inp_city_id"));
				x.clear();
				Thread.sleep(500);
				x.sendKeys(CITY);
				Thread.sleep(4000);
				fluentWait(By.xpath("//td[contains(.,'" + CITY + "')]")).click();
				Thread.sleep(500);
				test.get().pass("State/Province" + CITY);
				Thread.sleep(2000);
			}
            
            if (!MAXIMUM_RADIUS.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_max_radius"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), MAXIMUM_RADIUS);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, MAXIMUM_RADIUS);
                test.get().pass("<small>[Textfield]</small>Maximum Radius : " + val);
                Thread.sleep(500);
            }
            
            
            if(!TIME_SETTING.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_gmt_id"))).selectByVisibleText(TIME_SETTING);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Time Setting : " + TIME_SETTING);
            }
            
            
            if (!DESTINATION.equalsIgnoreCase("")) {
				WebElement x = fluentWait(By.id("inp_destination_code"));
				x.clear();
				Thread.sleep(500);
				x.sendKeys(DESTINATION);
				Thread.sleep(4000);
				fluentWait(By.xpath("//td[contains(.,'" + DESTINATION + "')]")).click();
				Thread.sleep(500);
				test.get().pass("Destination" + DESTINATION);
				Thread.sleep(2000);
			}

            
            WebElement submit = fluentWait(By.id("btn_a_1"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}

