package source.dataon.setting.timeatt_setting.leave_entitlement;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddLeaveEntitlementByMonth extends BaseTest {
    private static String file_location = curr + "//test-data//Add Leave Entitlement By Month.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_AddLeaveEntitlementByMonth
    	(String TC_ID, String LEAVE_CODE, String EFFECTIVE_DATE, String PROPORSIONAL_FLAG, 
    			String TEST_CASE_TYPE)
    throws Exception
    {
    	String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************ADD MASTER TASK*************
            checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_11']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("Time & Attendance Setting"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Leave Entitlement"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | Time & Attendance Setting | Leave Entitlement", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu2);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);

            WebElement ADD = fluentWait(By.id("ADD"));
            ADD.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Add");
            driver.switchTo().parentFrame();
            Thread.sleep(5000);
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
            test.get().pass("Menampilkan Form Leave Grade", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());
            
            
            if(!LEAVE_CODE.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_leave_code"))).selectByVisibleText(LEAVE_CODE);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Order No : " + LEAVE_CODE);
            }
            
            if (!EFFECTIVE_DATE.equalsIgnoreCase("")) {
				WebElement x = fluentWait(By.id("cal_effective_date"));
				x.clear();
				x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), EFFECTIVE_DATE);
				String val = x.getAttribute("value");
				Assert.assertEquals(val, EFFECTIVE_DATE);
				test.get().pass("<small>[Textfield]</small> Effective Date : " + val);
				Thread.sleep(500);
			}
            
            if(!PROPORSIONAL_FLAG.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_proportional_flag' and @title='" + PROPORSIONAL_FLAG + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, PROPORSIONAL_FLAG);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }

            
            WebElement submit = fluentWait(By.id("btn_a_1"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}

