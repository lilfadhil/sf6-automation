package source.dataon.setting.leave_setting.add_leave_setting;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.setup.BaseTest;
import com.setup.ReadExcel;

public class TAddLeaveValidRemainingLeave extends BaseTest {
    private static String file_location = curr + "//test-data//Add Leave Valid Remaining Leave.xls";
    private static String sheet_name = "Test Data";
    private static ReadExcel re = new ReadExcel();

    @Test(dataProvider = "ReadData")
    public void TC_AddLeaveValidRemainingLeave
    	(String TC_ID, String LEAVE_CODE, String LANG, String LEAVE_NAME, String ELIGIBILITY_LEAVE, String LEAVE_DAY_TYPE, String LEAVE_ENTITLEMENT, String AVAILABLE_AFTER, String LEAVE_VALID,
    			String LEAVE_ENTITLEMENT_ROUNDING, String ATTENDANCE_STATUS, String EMPLOYEE,
    			String TEST_CASE_TYPE)
    throws Exception
    {
    	String SC_TYPE = "vertical"; /*nonreport*/
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        setTestName(driver.getCurrentUrl(), TC_ID, methodName, TEST_CASE_TYPE);
        try
        {
        	//*************ADD MASTER TASK*************
            checkPageIsReady();
            driver.switchTo().defaultContent();
            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='menu_11']//a[@class='menulink']")));
            WebElement menu = fluentWait(By.xpath("//li[@id='menu_11']//a[@class='menulink']")); // Setting
            Actions actions = new Actions(driver);
            actions.moveToElement(menu).perform();
            Thread.sleep(500);
            WebElement submenu1 = fluentWait(By.linkText("Time & Attendance Setting"));
            Actions subactions1 = new Actions(driver);
            subactions1.moveToElement(submenu1).perform();
            Thread.sleep(500);
            WebElement submenu2 = fluentWait(By.linkText("Leave Setting"));
            Actions subactions2 = new Actions(driver);
            subactions2.moveToElement(submenu2).perform();
            Thread.sleep(500);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_1");
            test.get().pass("Menuju ke menu Setting | Time & Attendance Setting | Leave Setting", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_1.png").build());
            executor.executeScript("arguments[0].click();", submenu2);
            checkPageIsReady();
            Thread.sleep(2000);
            driver.switchTo().parentFrame();
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("frmSFBody"))); //Tampilan isi
            //fluentWait(By.id("RELOAD")).click();
            Thread.sleep(2000);

            WebElement ADD = fluentWait(By.id("ADD"));
            ADD.click();
            Thread.sleep(2000);
            test.get().pass("<small>[button]</small> Klik tombol Add");
            driver.switchTo().parentFrame();
            Thread.sleep(5000);
            takeScreenShot(TC_ID, SC_TYPE, TC_ID + "_2");
            test.get().pass("Menampilkan Form Leave Setting", MediaEntityBuilder.createScreenCaptureFromPath(filePathSc + TC_ID + "_2.png").build());
            
            if (!LEAVE_CODE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_leave_code"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), LEAVE_CODE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, LEAVE_CODE);
                test.get().pass("<small>[Textfield]</small>Task Code : " + val);
                Thread.sleep(500);
            }
            
            if (!LEAVE_NAME.equalsIgnoreCase("")) {
			    String[] st_desc = LEAVE_NAME.split(", ");
			    String[] lang = LANG.split(", ");
			    for(int i = 0;i < st_desc.length;i++)
			    {
			        WebElement x = fluentWait(By.id("inp_leavename_" + lang[i]));
			        x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), st_desc[i]);
			        Thread.sleep(500);
			        String val = x.getAttribute("value");
			        Assert.assertEquals(val, st_desc[i]);
			        test.get().pass("<small>[Input Textfield]</small> Task Name : " + st_desc[i]);
			    }
			    Thread.sleep(1000);
			}
            
            if (!ELIGIBILITY_LEAVE.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_eligibility_formula"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), ELIGIBILITY_LEAVE);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, ELIGIBILITY_LEAVE);
                test.get().pass("<small>[Textfield]</small>Eligibility : " + val);
                Thread.sleep(500);
            }
            
            if(!LEAVE_DAY_TYPE.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_daytype1' and @title='" + LEAVE_DAY_TYPE + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, LEAVE_DAY_TYPE);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }
            
            if(!LEAVE_ENTITLEMENT.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_entitlementcriteria"))).selectByVisibleText(LEAVE_ENTITLEMENT);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Leave Entitlement : " + LEAVE_ENTITLEMENT);
            }
            
            if (!AVAILABLE_AFTER.equalsIgnoreCase(""))
            {
            	WebElement x = fluentWait(By.id("inp_available_after"));
                x.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), AVAILABLE_AFTER);
                String val = x.getAttribute("value");
                Assert.assertEquals(val, AVAILABLE_AFTER);
                test.get().pass("<small>[Textfield]</small>AVAILABLE AFTER : " + val);
                Thread.sleep(500);
            }
            
            if(!LEAVE_VALID.equalsIgnoreCase(""))
            {
                WebElement x = fluentWait(By.xpath("//input[@id='inp_validcriteria' and @title='" + LEAVE_VALID + "']"));
                x.click();
                String value = x.getAttribute("title");
                Assert.assertEquals(value, LEAVE_VALID);
                test.get().pass("<small>[Radiobutton]</small> Report Type : " + value);
                Thread.sleep(500);
            }
            
            
            if(!LEAVE_ENTITLEMENT_ROUNDING.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_roundingtype"))).selectByVisibleText(LEAVE_ENTITLEMENT_ROUNDING);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Leave Entitlement : " + LEAVE_ENTITLEMENT_ROUNDING);
            }
            
            if(!ATTENDANCE_STATUS.equalsIgnoreCase(""))
            {
                new Select(fluentWait(By.name("inp_attend_code"))).selectByVisibleText(ATTENDANCE_STATUS);
                Thread.sleep(500);
                test.get().pass("<small>[Selectbox]</small> Leave Entitlement : " + ATTENDANCE_STATUS);
            }
            
            if (!EMPLOYEE.equalsIgnoreCase("")) {

				String[] typeofL = EMPLOYEE.split(", ");
				int num = 1;
				for (String eltypeofL : typeofL) {
					if (eltypeofL != null && eltypeofL.length() > 0) {
						WebElement tol = fluentWait(By.id("inp_empMember"));
						tol.sendKeys("");
						tol.sendKeys(eltypeofL);
						Thread.sleep(5000);
						WebElement found = fluentWait(By.cssSelector("#unselinp_empMember > option:nth-child(1)"));
						actions.doubleClick(found).perform();
						Thread.sleep(3000);
						tol.clear();
						Thread.sleep(500);
						test.get().pass("<small>[Multiple Selectbox]</small> Type of Leave" + num + " : " + eltypeofL);
					}
					num++;
				}
			}
			Thread.sleep(1000);
            
            WebElement submit = fluentWait(By.id("btn_a_1"));
            submit.click();
            Thread.sleep(4000);
            test.get().pass("<small>[Button]</small> Mengklik Submit");
            //handle alert
            resolveAllAlerts(driver, 16, true);
            Thread.sleep(4000);

        }
        catch (ElementNotVisibleException e) {
            Assert.fail("Not Found Element/Page");
        }
    }

    @DataProvider(name = "ReadData")
    private static Object[][] readData() {
        Object[][] arrObject = re.getExcelData(file_location, sheet_name);
        return arrObject;
    }

}

